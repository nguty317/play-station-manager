package Database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to represent a PlayStation user.
 * Created for Data Structures, SP2 2017
 * @author James Baumeister
 * @version 1.0
 */
public class User {
	private String username;
	private int level;
	private double key;
	private ArrayList<Trophy> trophies;
	private GameList games;
	private Calendar dob;
	private User left;
	private User right;
	private int balance;

	public User(String username, Calendar dob, int level) {
		this.username = username;
		this.dob = dob;
		this.level = level;
		this.key = calculateKey();
		this.balance = 0;
	}

	/**
	 * DO NOT MODIFY THIS METHOD
	 * This method uses the username and level to create a unique key.
	 * As we don't want the username's hash to increase the level, it's first converted
	 * to a floating point, then added to the level.
	 * @return the unique key
	 */
	public double calculateKey() {
		int hash = Math.abs(username.hashCode());
		// Calculate number of zeros we need
		int length = (int)(Math.log10(hash) + 1);
		// Make a divisor 10^x
		double divisor = Math.pow(10, length);
		// Return level.hash
		return level + hash / divisor;
	}

	public String toString() {
		Date date = dob.getTime();
		DateFormat format = new SimpleDateFormat("MMM dd, yyyy");
		String dobString = format.format(date);
		StringBuilder result = new StringBuilder();
		result.append("User: ").append(this.username).append("\n\nTrophies: \n");
		for (Trophy trophy : trophies) {
			result.append(trophy.toString()).append("\n");
		}
		result.append("\nGames: \n").append(games.toString());
		result.append("\n\nBirth Date: ").append(dobString);
		return result.toString();
	}

	public String getUsername() {
		return username;
	}

	public Calendar getDob() {
		return dob;
	}

	public int getLevel() {
		return level;
	}

	public double getKey() {
		return key;
	}

	public void setGames(GameList games) {
		this.games = games;
	}

	public void setTrophies(ArrayList<Trophy> trophies) {
		this.trophies = trophies;
	}

	public User getLeft() {
		return left;
	}

	public User getRight() {
		return right;
	}

	public void setLeft(User faust) {
		this.left = faust;
	}

	public void setRight(User pippin) {
		this.right = pippin;
	}

	public ArrayList<Trophy> getTrophies() {
		return trophies;
	}

	public GameList getGames() {
		return games;
	}

	public void levelUp() {
		this.level += 1;
		this.key = calculateKey();
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
