package Database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class to represent a PlayStation game.
 * Created for Data Structures, SP2 2017
 * @author James Baumeister
 * @version 1.0
 */
public class Game {
	private String name;
	private Calendar released;
	private int totalTrophies;
	private Game nextGame;

    public Game() {
    	this.name = null;
    	this.released = null;
    	this.totalTrophies = 0;
	}

    public Game(String name, Calendar released, int totalTrophies) {
		this.name = name;
		this.released = released;
		this.totalTrophies = totalTrophies;
    }

	public Calendar getReleased() {
		return this.released;
	}

	public int getTotalTrophies() {
		return this.totalTrophies;
	}

	public String getName() {
		return this.name;
	}

	public void setNext(Game g2) {
		this.nextGame = g2;
	}

	public Game getNext() {
    	return this.nextGame;
	}

	@Override
	public String toString() {
		DateFormat format = new SimpleDateFormat("MMM dd, yyyy");
		return "\"" + name + "\", released on: " + format.format(released.getTime());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Game)) return false;
		Game game = (Game) o;
		return getTotalTrophies() == game.getTotalTrophies() &&
				getName().equals(game.getName()) &&
				getReleased().equals(game.getReleased());
	}
}
