package Database;

/**
 * Class to represent a single linked-list of Database.Game objects.
 * Created for Data Structures, SP2 2017
 *
 * @author James Baumeister
 * @version 1.0
 */
public class GameList {

    public Game head;

    public GameList(Game head) {
        this.head = head;
    }

    public String toString() {
        if (this.head == null) {
            return "Empty game list";
        }
        StringBuilder result = new StringBuilder();
        Game temp = this.head;
        while (temp.getNext() != null) {
            result.append(temp.toString()).append("\n");
            temp = temp.getNext();
        }
        result.append(temp.toString());
        return result.toString();
    }

    public Game getGame(String name) {
        if (name == null || name.equals("")) {
            throw new IllegalArgumentException();
        } else {
            Game temp = this.head;
            // Traversing through game list.
            while (temp.getNext() != null) {
                if (temp.getName().equals(name)) {
                    return temp;
                } else {
                    temp = temp.getNext();
                }
            }
            if (temp.getName().equals(name)) {
                return temp;
            } else {
                return null;
            }
        }
    }

    public void addGame(Game massEffect) {
        if (massEffect == null) {
            throw new IllegalArgumentException();
        } else {
            if (this.head == null) {
                this.head = massEffect;
            } else {
                Game temp = this.head;
                // Traversing through game list.
                while (temp.getNext() != null) {
                    if (!temp.equals(massEffect)) {
                        temp = temp.getNext();
                        // If game has already existed, return.
                    } else {
                        return;
                    }
                }
                // Add a new game to game list.
                if (!temp.equals(massEffect)) {
                    temp.setNext(massEffect);
                }
            }
        }
    }

    public void removeGame(String name) {
        if (name == null || name.equals("")) {
            throw new IllegalArgumentException();
        } else {
            Game temp = this.head;
            if (temp.getName().equals(name)) {
                if (temp.getNext() == null) {
                    this.head = null;
                    return;
                } else {
                    this.head = temp.getNext();
                    return;
                }
            }
            // Traversing through game list.
            while (temp.getNext() != null) {
                if (temp.getNext().getName().equals(name)) {
                    temp.setNext(temp.getNext().getNext());
                    return;
                } else {
                    temp = temp.getNext();
                }
            }
        }
    }

    public void removeGame(Game game) {
        if (game == null) {
            throw new IllegalArgumentException();
        } else {
            Game temp = this.head;
            if (temp.equals(game)) {
                if (temp.getNext() == null) {
                    this.head = null;
                    return;
                } else {
                    this.head = temp.getNext();
                    return;
                }
            }
            // Traversing through game list.
            while (temp.getNext() != null) {
                if (temp.getNext().equals(game)) {
                    temp.setNext(temp.getNext().getNext());
                    return;
                } else {
                    temp = temp.getNext();
                }
            }
        }
    }
}

