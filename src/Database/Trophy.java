package Database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to represent a PlayStation game trophy. A trophy comes in
 * four ranks: bronze, silver, gold and platinum. The date the trophy was
 * earned and its respective game is also stored.
 * Created for Data Structures, SP2 2017
 * @author James Baumeister
 * @version 1.0
 */
public class Trophy {
	private String name;
	private Rank rank;
	private Rarity rarity;
	private Calendar obtained;
	private Game game;

	public enum Rank {
		BRONZE, GOLD, SILVER, PLATINUM

	}

	public enum Rarity {
		COMMON, UNCOMMON, RARE, VERY_RARE, ULTRA_RARE

	}

	public Trophy() {
		this.name = null;
		this.rank = null;
		this.rarity = null;
		this.obtained = null;
		this.game = null;
	}

    public Trophy(String name, Rank rank, Rarity rarity, Calendar obtained, Game game) {
		this.name = name;
		this.rank = rank;
		this.rarity = rarity;
		this.obtained = obtained;
		this.game = game;
	}

    public String toString() {
		DateFormat format = new SimpleDateFormat("MMM dd, yyyy");
		return "\"" + name + "\", rank: "
					+ rank.name() + ", rarity: "
					+ rarity.name() + ", obtained on: "
					+ format.format(obtained.getTime());
    }

	public String getName() {
		return name;
	}

	public Rank getRank() {
		return rank;
	}

	public Rarity getRarity() {
		return rarity;
	}

	public Calendar getObtained() {
		return obtained;
	}

	public Game getGame() {
		return game;
	}
}
