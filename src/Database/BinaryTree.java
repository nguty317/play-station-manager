package Database;

import java.util.*;

/**
 * Uses a binary search tree to store a database of
 * PlayStation users. Nodes are ordered by user unique key (see the
 * User class for more detail).
 * Created for Data Structures, SP2 2017
 *
 * @author James Baumeister
 * @version 1.0
 */
public class BinaryTree {

    public User root;   // Variable for root user.
    private boolean increase;    // Variable for whether if the tree's height has increased.
    private boolean addReturn;

    /**
     * Making new friends is great. This method should add your new
     * bestie to your database (tree). Remember that they should be
     * added according to their key.
     *
     * @param friend The friend to be added
     * @return true if  successfully added, false for all error cases
     * @throws IllegalArgumentException if friend is null
     */
    public boolean beFriend(User friend) throws IllegalArgumentException {
        if (friend == null) {
            throw new IllegalArgumentException();
        } else {
            // Variable to point to temporary user, starting at root user.
            User temp = this.root;
            // Variable to point to parent user of new user.
            User parent = null;

            // If tree is empty, make new user into root user.
            if (temp == null) {
                this.root = friend;
                return true;
            } else {
                // If new user is the same key as root user, return false.
                if (temp.getKey() == friend.getKey()) {
                    return false;
                }

                // Loop to find new user's parent user.
                while (temp != null) {
                    parent = temp;

                    // If new user's key is smaller than temp user's key, go to left user.
                    if (friend.getKey() < temp.getKey()) {
                        // Check if the new user is the same as left child of temp user or not.
                        // If yes, return false.
                        if (temp.getLeft() != null && temp.getLeft().getKey() == friend.getKey()) {
                            return false;
                        }
                        // If not, assign temp user to the left child of current temp user.
                        temp = temp.getLeft();

                        // If new user's key is bigger than temp user's key, go to right user.
                    } else if (friend.getKey() > temp.getKey()) {
                        // Check if the new user is the same as right child of temp user or not.
                        // If yes, return false.
                        if (temp.getRight() != null && temp.getRight().getKey() == friend.getKey()) {
                            return false;
                        }
                        // If not, assign temp user to the right child of current temp user.
                        temp = temp.getRight();

                        // If new user's key is same as temp user's key, return false.
                    } else {
                        return false;
                    }
                }

                // Add new user to the right/left of the parent user.
                // If new user's key is smaller than its parent's key, add to the left.
                if (friend.getKey() < parent.getKey()) {
                    parent.setLeft(friend);
                    // If new user's key is bigger than its parent's key, add to the right.
                } else if (friend.getKey() > parent.getKey()) {
                    parent.setRight(friend);
                    // If new user's key is same as its parent's key, return false.
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Sometimes friendships don't work out. In those cases it's best
     * to remove that "friend" altogether. This method should remove
     * all trace of that "friend" in the database (tree).
     *
     * @param friend the "friend" to remove
     * @return true if successfully removed, false for all error cases
     * @throws IllegalArgumentException if "friend" is null
     */
    public boolean deFriend(User friend) throws IllegalArgumentException {
        if (friend == null) {
            throw new IllegalArgumentException();
        } else {
            if (this.root == null) {
                return false;
            } else {
                // Variable to point to temporary user, starting at root user.
                User temp = this.root;

                // Loop to find deleting user.
                while (temp != null) {

                    // If new user's key is smaller than temp user's key, go to left user.
                    if (friend.getKey() < temp.getKey()) {
                        // Check if the new user is the same as left child of temp user or not.
                        // If yes, remove temp user's left user and find temp user's new left user.
                        if (temp.getLeft() != null && temp.getLeft().getKey() == friend.getKey()) {
                            temp.setLeft(findReplacementUser(temp.getLeft()));
                            return true;
                        }
                        temp = temp.getLeft();

                        // If new user's key is bigger than temp user's key, go to right user.
                    } else if (friend.getKey() > temp.getKey()) {
                        // Check if the new user is the same as right child of temp user or not.
                        // If yes, remove temp user's right user and find temp user's new right user.
                        if (temp.getRight() != null && temp.getRight().getKey() == friend.getKey()) {
                            temp.setRight(findReplacementUser(temp.getRight()));
                            return true;
                        }
                        // If not, assign temp user to the right child of current temp user.
                        temp = temp.getRight();

                        // If new user's key is same as temp user's key, find new root user.
                    } else {
                        this.root = findReplacementUser(temp);
                        return true;
                    }
                }
                return false;
            }
        }
    }

    /**
     * In your quest to be the very best you need to know how many
     * of your friends are ranked higher than you. This method should
     * return the number of higher ranked users that the provided reference
     * user, or zero if there are none (woot!).
     *
     * @param reference The starting point in the search
     * @return Number of higher ranked users or -1 if user not found
     * @throws IllegalArgumentException if reference is null
     */
    public int countBetterPlayers(User reference) throws IllegalArgumentException {
        if (reference == null) {
            throw new IllegalArgumentException();
        } else {
            int count = 0;    // Variable for number of better players.
            boolean exist = false;  // Variable for whether if the referenced player exist in the tree.
            // If the tree is null.
            if (this.root == null) {
                return -1;
            }
            Stack<User> userStack = new Stack<User>();  // Stack to store users.
            User temp = this.root;  // Temp user to traverse the tree.
            // Traverse through the tree.
            while (temp != null || userStack.size() > 0) {
                while (temp != null) {
                    userStack.push(temp);
                    temp = temp.getLeft();
                }
                temp = userStack.pop();
                // If the referenced player exist.
                if (temp.getKey() == reference.getKey()) {
                    exist = true;
                }
                // If level is bigger, add to the number of better players.
                if (temp.getLevel() > reference.getLevel()) {
                    count++;
                }
                temp = temp.getRight();
            }
            // If the referenced user exists, return the result.
            if (exist) {
                return count;
            } else {
                return -1;
            }
        }
    }

    /**
     * Bragging rights are well earned, but it's good to be sure that you're actually
     * better than those over whom you're lording your achievements. This method
     * should find all those friends who have a lower level than you, or zero if
     * there are none (you suck).
     *
     * @param reference The starting point in the search
     * @return Number of lower ranked users
     * @throws IllegalArgumentException if reference is null
     */
    public int countWorsePlayers(User reference) throws IllegalArgumentException {
        if (reference == null) {
            throw new IllegalArgumentException();
        } else {
            int count = 0;     // Variable for number of worse players.
            boolean exist = false;      // Variable for whether if the referenced player exist in the tree.
            // If the tree is null.
            if (this.root == null) {
                return -1;
            }
            Stack<User> userStack = new Stack<User>();      // Stack to store users.
            User temp = this.root;      // Temp user to traverse the tree.
            // Traverse through the tree.
            while (temp != null || userStack.size() > 0) {
                while (temp != null) {
                    userStack.push(temp);
                    temp = temp.getLeft();
                }
                temp = userStack.pop();
                // If the referenced player exist.
                if (temp.getKey() == reference.getKey()) {
                    exist = true;
                }
                // If level is smaller, add to the number of worse players.
                if (temp.getLevel() < reference.getLevel()) {
                    count++;
                }
                temp = temp.getRight();
            }
            // If the referenced user exists, return the result.
            if (exist) {
                return count;
            } else {
                return -1;
            }
        }
    }

    /**
     * The best player may not necessarily be measured by who has the highest level.
     * Platinum trophies are the holy grail, so it would be good to know who has the
     * most. This method should return the user with the highest number of platinum trophies.
     *
     * @return the user with the most platinum trophies, or null if there are none
     */
    public User mostPlatinums() {
        if (this.root == null) {
            return null;
        }
        Stack<User> userStack = new Stack<User>();      // Stack to store users.
        User temp = this.root;      // Variable to store temp user.
        User mostPlatinum = null;       // Variable to store user with the most platinum trophies.
        int platinumCount = 0;  // Variable for number of trophies.
        // Count number of platinum trophies of root user.
        for (Trophy trophy : temp.getTrophies()) {
            if (trophy.getRank().equals(Trophy.Rank.PLATINUM)) {
                platinumCount++;
            }
        }

        // Traverse the tree.
        while (temp != null || userStack.size() > 0) {
            while (temp != null) {
                userStack.push(temp);
                temp = temp.getLeft();
            }
            temp = userStack.pop();

            int count = 0;
            // Count number of platinum trophies of temp user.
            for (Trophy trophy : temp.getTrophies()) {
                if (trophy.getRank().equals(Trophy.Rank.PLATINUM)) {
                    count++;
                }
            }
            // If the number is higher, replace user with the most platinum trophies.
            if (count > platinumCount) {
                mostPlatinum = temp;
            }

            temp = temp.getRight();
        }
        return mostPlatinum;

    }

    /**
     * You or one of your friends bought a new game! This method should add it to their
     * GameList.
     *
     * @param username The user who has bought the game
     * @param game     The game to be added
     */
    public void addGame(String username, Game game) throws IllegalArgumentException {
        if (username == null || username.equals("") || game == null) {
            throw new IllegalArgumentException();
        } else {
            // Find user.
            User user = findUser(username);
            // Add game.
            if (user != null) {
                GameList gameList = user.getGames();
                gameList.addGame(game);
            }
        }
    }

    /**
     * You or one of your friends achieved a new trophy! This method should add it to
     * their trophies.
     *
     * @param username The user who has earned a new trophy
     * @param trophy   The trophy to be added
     */
    public void addTrophy(String username, Trophy trophy) throws IllegalArgumentException {
        if (username == null || username.equals("") || trophy == null) {
            throw new IllegalArgumentException();
        } else {
            // Find user.
            User user = findUser(username);
            // Add tropy.
            if (user != null) {
                ArrayList<Trophy> trophies = user.getTrophies();
                trophies.add(trophy);
            }

        }
    }

    /**
     * You or one of your friends has gained one level! This is great news, except that
     * it may have ruined your tree structure! A node move may be in order.
     *
     * @param username The user whose level has increased
     */
    public void levelUp(String username) throws IllegalArgumentException {
        if (username == null || username.equals("")) {
            throw new IllegalArgumentException();
        } else {
            // Find user.
            User user = findUser(username);
            // Level up and rearrange tree.
            if (user != null) {
                this.deFriend(user);
                user.levelUp();
                this.beFriend(user);
            }
        }
    }

    /**
     * A nice, neat print-out of your friends would look great in the secret scrap-book
     * that you keep hidden underneath your pillow. This method should print out the
     * details of each user, traversing the tree in order.
     *
     * @return A string version of the tree
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        Stack<User> userStack = new Stack<User>();
        User temp = this.root;

        // Traversing through tree.
        while (temp != null || userStack.size() > 0) {
            while (temp != null) {
                userStack.push(temp);
                temp = temp.getLeft();
            }
            temp = userStack.pop();
            result.append(temp.toString()).append("\n");
            temp = temp.getRight();
        }
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    /**
     * As your friends list grows, adding with regular binary tree rules will
     * result in an unbalanced and inefficient tree. One approach to fix this
     * is to implement an add method that uses AVL balancing. This method should
     * work in the same way as beFriend, but maintain a balanced tree according to
     * AVL rules.
     *
     * @param friend The friend to be added
     * @return true if  successfully added, false for all error cases
     * @throws IllegalArgumentException if friend is null
     */
    public boolean addAVL(User friend) throws IllegalArgumentException {
        if (friend == null) {
            throw new IllegalArgumentException();
        } else {
            increase = false;
            // Add user using AVL tree insertion method.
            this.root = add(root, friend);
            return addReturn;
        }
    }

    /**
     * Add user into a tree using AVL tree insertion method.
     *
     * @param localRoot The root User in a tree/subtree
     * @param user The User that needs to be added
     * @return User The added User
     */
    private User add(User localRoot, User user) {
        if (localRoot == null) {
            addReturn = true;
            increase = true;
            return user;
        }
        boolean addLeft = true;     // Variable to check which side is an user added to.
        if (user.getKey() == localRoot.getKey()) {
            // Item is already in the tree.
            increase = false;
            addReturn = false;
            return localRoot;
            // Add to the left subtree.
        } else if (user.getKey() < localRoot.getKey()) {
            localRoot.setLeft(add(localRoot.getLeft(), user));
            addLeft = true;
            // Add to the right subtree.
        } else {
            localRoot.setRight(add(localRoot.getRight(), user));
            addLeft = false;
        }

        // If the tree's height increased, change balance of local root user.
        if (increase) {
            if (addLeft) {
                localRoot.setBalance(localRoot.getBalance() - 1);
            } else {
                localRoot.setBalance(localRoot.getBalance() + 1);
            }
            if (localRoot.getBalance() == 0) {
                increase = false;
            }

            // If the balance < -1 or > 1, rebalance tree.
            if (localRoot.getBalance() < -1) {
                increase = false;
                return rebalanceLeft(localRoot);
            } else if (localRoot.getBalance() > 1) {
                increase = false;
                return rebalanceRight(localRoot);
            }
        }
        return localRoot;
    }

    /**
     * Rebalance a tree after user insertion and the tree is left heavy.
     *
     * @param localRoot The root User in a tree/subtree
     * @return User The replacement for root user
     */
    private User rebalanceLeft(User localRoot) {    
        // Obtain reference to left child.    
        User leftChild = localRoot.getLeft();
        // See whether left‐right heavy.    
        if (leftChild.getBalance() > 0) {
            // Obtain reference to left‐right child.
            User leftRightChild = leftChild.getRight();
            // Adjust the balances to be their new values after the rotations are performed.

            if (leftRightChild.getBalance() < 0) {
                leftChild.setBalance(0);
                leftRightChild.setBalance(0);
                localRoot.setBalance(1);
            } else {
                leftChild.setBalance(-1);
                leftRightChild.setBalance(0);
                localRoot.setBalance(0);
            }        // Perform left rotation.
            localRoot.setLeft(rotateLeft(leftChild));
        } else {
                // In this case the leftChild (the new root) and the root
                // (new right child) will both be balanced after the rotation.
            leftChild.setBalance(0);
            localRoot.setBalance(0);
        }    // Now rotate the local root right.
        return rotateRight(localRoot);
    }

    /**
     * Rebalance a tree after user insertion and the tree is right heavy.
     *
     * @param localRoot The root User in a tree/subtree
     * @return User The replacement for root user
     */
    private User rebalanceRight(User localRoot) {
        // Obtain reference to right child.
        User rightChild = localRoot.getRight();
        // See whether right-left heavy.
        if (rightChild.getBalance() < 0) {
            // Obtain reference to left‐right child.
            User rightLeftChild = rightChild.getLeft();
            // Adjust the balances to be their new values after the rotations are performed.

            if (rightLeftChild.getBalance() < 0) {
                rightChild.setBalance(0);
                rightLeftChild.setBalance(0);
                localRoot.setBalance(1);
            } else {
                rightChild.setBalance(-1);
                rightLeftChild.setBalance(0);
                localRoot.setBalance(0);
            }        // Perform right rotation.
            localRoot.setRight(rotateRight(rightChild));
        } else {
            // In this case the rightChild (the new root) and the root
            // (new left child) will both be balanced after the rotation.
            rightChild.setBalance(0);
            localRoot.setBalance(0);
        }    // Now rotate the local root left.
        return rotateLeft(localRoot);
    }

    /**
     * Find the replacement User for an User which is going to be removed.
     *
     * @param user The User that is being removed
     * @return User The replacement for removed User
     */
    private User findReplacementUser(User user) {
        // If removing user has no children, return null.
        if (user.getLeft() == null && user.getRight() == null) {
            return null;

            // If removing user has left children only, return the removing user's left user.
        } else if (user.getLeft() != null && user.getRight() == null) {
            User left = user.getLeft();
            user.setLeft(null);
            return left;

            // If removing user has right children only, return the removing user's right user.
        } else if (user.getLeft() == null && user.getRight() != null) {
            User right = user.getRight();
            user.setRight(null);
            return right;

            // If removing user has both left and right children.
        } else {
            // If removing user's left user has no right children, return removing user's left user.
            if (user.getLeft().getRight() == null) {
                user.getLeft().setRight(user.getRight());
                User left = user.getLeft();
                user.setRight(null);
                user.setLeft(null);
                return left;
                // If removing user's left user has right children, return the most right user in the tree which removing user's left user is the local root
                // AKA find the user with the nearest (but smaller) key after removing that one user.
            } else {
                User temp = user.getLeft();
                while (temp.getRight().getRight() != null) {
                    temp = temp.getRight();
                }
                User parent = temp;
                temp = temp.getRight();
                parent.setRight(null);
                temp.setLeft(user.getLeft());
                temp.setRight(user.getRight());
                user.setLeft(null);
                user.setRight(null);
                return temp;
            }
        }
    }

    /**
     * Find an User using their username.
     *
     * @param username Username of the User we're looking for
     * @return User The User we are looking for
     */
    private User findUser(String username) {
        if (this.root == null) {
            return null;
        }
        Stack<User> userStack = new Stack<User>();
        User temp = this.root;

        while (temp != null || userStack.size() > 0) {
            while (temp != null) {
                userStack.push(temp);
                temp = temp.getLeft();
            }
            temp = userStack.pop();
            if (temp.getUsername().equals(username)) {
                return temp;
            }
            temp = temp.getRight();
        }
        return null;
    }

    /**
     * Perform a right rotation.
     *
     * @param localRoot The root user of a tree/subtree
     * @return User The new root User
     */
    private User rotateRight(User localRoot) {
        User temp = localRoot.getLeft();
        localRoot.setLeft(temp.getRight());
        temp.setRight(localRoot);
        return temp;
    }

    /**
     * Perform a left rotation.
     *
     * @param localRoot The root user of a tree/subtree
     * @return User The new root User
     */
    private User rotateLeft(User localRoot) {
        User temp = localRoot.getRight();
        localRoot.setRight(temp.getLeft());
        temp.setLeft(localRoot);
        return temp;
    }
}
